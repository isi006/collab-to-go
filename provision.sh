#!/usr/bin/env bash

SYSTEM_NAME=$1
EMAIL=$2

COMPOSE_VERSION=1.25.3

export DEBIAN_FRONTEND=noninteractive

apt-get update -y

# install docker
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    git \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update -y

apt-get install -y docker-ce docker-ce-cli containerd.io

curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

git clone https://github.com/jitsi/docker-jitsi-meet
cd docker-jitsi-meet

cp env.example .env
./gen-passwords.sh
mkdir -p /data/${SYSTEM_NAME}/config/{web/letsencrypt,transcripts,prosody,jicofo,jvb,jigasi,jibri}


cat <<EOT >> .env
CONFIG=/data/${SYSTEM_NAME}/config
HTTP_PORT=80
HTTPS_PORT=443
TZ=Europe/Berlin
PUBLIC_URL=https://${SYSTEM_NAME}
ENABLE_LETSENCRYPT=1
LETSENCRYPT_DOMAIN=${SYSTEM_NAME}
LETSENCRYPT_EMAIL=${EMAIL}
EOT

docker-compose up -d
