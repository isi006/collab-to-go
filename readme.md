# Collaboration to go

The need of online collaboration is omnipresent at this time.

So I decided to offer an application stack for this which has the following design goals:  

* inexpensive to host
* completely free software
* lightweight
* secure
* easy to use
* create and destroy on demand without data loss
* keep it as simple as possible, no cloud provider independence

The first (and yet only) component ist https://jitsi.org/. I plan to add a chat solution (e. g. RocketChat or Mattermost) in the future.

## Prerequisits

* Installed Terraform (https://www.terraform.io/)
* A Hetzner account (https://www.hetzner.de/)
* A google cloud console project (https://console.cloud.google.com/)

## Configuration

Copy the configuration template:

    cp terraform.tfvars_template terraform.tfvars

And configure your values according the documentation in the file.

Place a access file in the project dir named `google-cloud.json` of a google service account, that has the rights to manage your Cloud-DNS zone.

## Create a new system

1. Create a new terraform workspace and initialize it:
```
    terraform workspace new <workspace_name>
    terraform init
```
2. Optional: plan the creation and check what terraform will do to create the system
```
    terraform plan
```
3. Apply the system creation
```
    terraform apply
```

The creation takes 2-3 minutes. After that, the system should be available under:

    https://<workspace_name>.<domain_name>

## Troubleshooting

If system not works as excepted check first your cloud consoles:

* Is the new VM in the Hetzner cloud console visible?
* Is the DNS A-record present in Google Cloud-DNS

Then ssh into the newly create VM (use your corresponding private ssh key):

    ssh root@<workspace_name>.<domain_name>

Here you can change to the folder `docker-jitsi-meet` and use docker-compose to examine the system, like

    docker-compose ps
    docker-compose logs -f

More infos about docker-compose: https://docs.docker.com/compose/

## Destroy a system

When you don't need the system (e. g. at night or at the weekend) destroy it! Frequently destroying and recreating the system prevents unnecessary hosting costs and keeps the system up to date.

Call

    terraform destroy

to completely delete the system. Recreate the system with `terraform apply` at any time you need it. The SSL certificates will be kept in the `config`-dir to avoid any rate limiting problems with letsencrypt.

## Create more systems

You can create as many terraform workspaces (`terraform workspace new ...`) as you want to host more systems. To switch between them use:

    terraform workspace select <workspace_name>

## Thank you...

... to jitsi and the awesome project https://github.com/jitsi/docker-jitsi-meet that have made this possible so quickly.

## Roadmap

* Keep the terraform state and the SSL certificates on a remote storage (Google cloud storage) to make the project more reliable and build pipeline friendly
* App a chat solution like RocketChat or Mattermost
* Using HashiCorps Packer (https://www.packer.io/) to prebuild the system image for quicker system creation and reducing network traffic.