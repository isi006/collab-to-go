# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "server_type" {
  type = string
  default = "cpx11"
}

variable "le_email" {
  type = string
}
variable "managed_zone" {
  type = string
}

locals {
  system_name = "${terraform.workspace}.${var.domain_name}"
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

provider "google" {
  credentials = file("google-cloud.json")
  project     = "isi-labs"
  region      = "europe-west3"
}

resource "google_dns_record_set" "node" {
  name = "${local.system_name}."
  type = "A"
  ttl  = 10

  managed_zone = var.managed_zone

  rrdatas = [ hcloud_server.node.ipv4_address ]
}

resource "hcloud_server" "node" {
  name = local.system_name
  image = "ubuntu-18.04"
  location = "fsn1"
  ssh_keys = [ "${var.ssh_key}" ]
  server_type = var.server_type

  connection {
    type     = "ssh"
    user     = "root"
    host     = self.ipv4_address
  }

  provisioner "file" {
    when = create
    source      = "provision.sh"
    destination = "/root/provision.sh"
  }

  provisioner "remote-exec" {
    when = create
    inline = [
      "mkdir -p /data/${self.name}/config/web"
    ]
  }
  provisioner "local-exec" {
    when = create
    command = "mkdir -p config/${self.name} && rsync -e \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" -rltpv --delete config/${self.name}/config/web/letsencrypt root@${self.ipv4_address}:/data/${self.name}/config/web"
  }

  provisioner "remote-exec" {
    when = create
    inline = [
      "chmod +x /root/provision.sh",
      "/root/provision.sh ${self.name} ${var.le_email}",
    ]
  }

  provisioner "local-exec" {
    when = destroy
    command = "rsync -e \"ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null\" -rltpv --delete root@${self.ipv4_address}:/data/${self.name}/config/web/letsencrypt config/${self.name}/config/web"
  }

}
